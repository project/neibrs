<?php

/**
 * @file
 * Contains ip.module.
 */

use Drupal\commerce_order\Entity\OrderType;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function ip_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ip module.
    case 'help.page.ip':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The server ip for idc industrial.') . '</p>';
      return $output;
    case 'entity.ip.add_form':
    case 'entity.ip.edit_form':
      $output = '';
      $output .= '<p>' . t('The inet ip must bind to specify server on seat of cabinet.') . '</p>';
      return $output;
    default:
  }
}

/**
 * Implements hook_theme().
 */
function ip_theme() {
  $theme = [];
  $theme['ip'] = [
    'render element' => 'elements',
    'file' => 'ip.page.inc',
    'template' => 'ip',
  ];
  $theme['ip_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'ip.page.inc',
  ];
  return $theme;
}

/**
* Implements hook_theme_suggestions_HOOK().
*/
function ip_theme_suggestions_ip(array $variables) {
  $suggestions = [];
  $entity = $variables['elements']['#ip'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'ip__' . $sanitized_view_mode;
  $suggestions[] = 'ip__' . $entity->bundle();
  $suggestions[] = 'ip__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'ip__' . $entity->id();
  $suggestions[] = 'ip__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_entity_extra_field_info().
 */
function ip_entity_extra_field_info() {
  $extra = [];
  foreach (OrderType::loadMultiple() as $bundle) {
    $extra['commerce_order'][$bundle->id()]['display']['ips'] = [
      'label' => t('IPs'),
      'description' => t('The ips belongs to current order.'),
      'weight' => 0,
    ];
  }

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function ip_commerce_order_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('ips')) {
    $build['ips'] = \Drupal::formBuilder()->getForm('\Drupal\ip\Form\IpWithSpecificOrderForm', $entity, $display);
  }
}
