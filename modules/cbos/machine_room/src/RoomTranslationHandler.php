<?php

namespace Drupal\machine_room;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for room.
 */
class RoomTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
