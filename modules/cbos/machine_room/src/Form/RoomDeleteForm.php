<?php

namespace Drupal\machine_room\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Machine Room.
 *
 * @ingroup machine_room
 */
class RoomDeleteForm extends ContentEntityDeleteForm {


}
