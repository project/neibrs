<?php

namespace Drupal\machine_room\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Machine Room type entities.
 */
interface RoomTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
