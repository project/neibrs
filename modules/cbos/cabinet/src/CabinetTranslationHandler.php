<?php

namespace Drupal\cabinet;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cabinet.
 */
class CabinetTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
