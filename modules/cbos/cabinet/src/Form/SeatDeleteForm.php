<?php

namespace Drupal\cabinet\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Seat.
 *
 * @ingroup cabinet
 */
class SeatDeleteForm extends ContentEntityDeleteForm {


}
