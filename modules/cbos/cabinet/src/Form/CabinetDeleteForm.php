<?php

namespace Drupal\cabinet\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cabinet.
 *
 * @ingroup cabinet
 */
class CabinetDeleteForm extends ContentEntityDeleteForm {


}
