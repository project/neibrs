<?php

namespace Drupal\cabinet;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for seat.
 */
class SeatTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
