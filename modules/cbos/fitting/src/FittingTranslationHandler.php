<?php

namespace Drupal\fitting;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for fitting.
 */
class FittingTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
