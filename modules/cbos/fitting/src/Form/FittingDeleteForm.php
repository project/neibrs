<?php

namespace Drupal\fitting\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fitting.
 *
 * @ingroup fitting
 */
class FittingDeleteForm extends ContentEntityDeleteForm {


}
