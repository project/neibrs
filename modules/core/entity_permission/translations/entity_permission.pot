# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  modules/eabax/core/entity_permission/entity_permission.module: n/a
#  modules/eabax/core/entity_permission/entity_permission.routing.yml: n/a
#  modules/eabax/core/entity_permission/entity_permission.info.yml: n/a
#  modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml: n/a
#  modules/eabax/core/entity_permission/src/Form/EntityPermissionSettingsForm.php: n/a
#  modules/eabax/core/entity_permission/entity_permission.permissions.yml: n/a
#  modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-05-24 17:48+0800\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: modules/eabax/core/entity_permission/entity_permission.module:140 modules/eabax/core/entity_permission/entity_permission.routing.yml:0
msgid "Edit entity permissions"
msgstr ""

#: modules/eabax/core/entity_permission/entity_permission.info.yml:0;0 modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0;0;0;0 modules/eabax/core/entity_permission/src/Form/EntityPermissionSettingsForm.php:33
msgid "Entity permission"
msgstr ""

#: modules/eabax/core/entity_permission/entity_permission.info.yml:0
msgid "Neibrs core"
msgstr ""

#: modules/eabax/core/entity_permission/entity_permission.permissions.yml:0
msgid "Administer entity permissions"
msgstr ""

#: modules/eabax/core/entity_permission/entity_permission.routing.yml:0 modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0
msgid "Entity permission settings"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0
msgid "Entities which controled by entity permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0;0 modules/eabax/core/entity_permission/src/Form/EntityPermissionSettingsForm.php:34 modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:109
msgid "Bundle permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0 modules/eabax/core/entity_permission/src/Form/EntityPermissionSettingsForm.php:35
msgid "Field permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0;0
msgid "View permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0
msgid "Update permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0
msgid "Delete permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0;0
msgid "Create permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0
msgid "Bundle field permission"
msgstr ""

#: modules/eabax/core/entity_permission/config/schema/entity_permission.schema.yml:0
msgid "Edit permission"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/EntityPermissionSettingsForm.php:32
msgid "Entity"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:33
msgid "Need to setup entities to manage permissions."
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:71;112;172;185
msgid "View"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:80;113
msgid "Update"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:89;114
msgid "Delete"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:98;115
msgid "Create"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:111
msgid "Bundle"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:162
msgid "Field permissions"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:171;184
msgid "Field"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:173;186
msgid "Edit"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:175
msgid "Base field permissions"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:188
msgid "@bundle field permissions"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:213
msgid "Save"
msgstr ""

#: modules/eabax/core/entity_permission/src/Form/RoleEntityPermissionForm.php:266
msgid "Entity permissions for @role has saved."
msgstr ""

